Source: luma.lcd
Maintainer: Debian Electronics Team <pkg-electronics-devel@alioth-lists.debian.net>
Uploaders: Anton Gladky <gladk@debian.org>
Section: libs
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-dev,
               python3-luma.core (>= 2.4.0),
               python3-pil,
               python3-pytest,
               python3-setuptools,
               python3-sphinx
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/electronics-team/luma.lcd
Vcs-Git: https://salsa.debian.org/electronics-team/luma.lcd.git
Homepage: https://github.com/rm-hull/luma.lcd

Package: python3-luma.lcd
Architecture: any
Section: python
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
         python3-luma.core (>= 2.4.0)
Description: library interfacing small LCD displays
 Library provides a Python 3 interface to small LCD displays connected to
 Raspberry Pi and other Linux-based single-board computers (SBC).
 It currently supports devices using the HD44780, PCD8544, ST7735, HT1621, and
 UC1701X controllers. It provides a Pillow-compatible drawing canvas, and other
 functionality to support:
    scrolling/panning capability,
    terminal-style printing,
    state management,
    color/greyscale (where supported),
    dithering to monochrome

Package: luma.lcd-doc
Architecture: all
Section: python
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Description: doc for library interfacing small LCD displays
 Library provides a Python 3 interface to small LCD displays connected to
 Raspberry Pi and other Linux-based single-board computers (SBC).
 It currently supports devices using the HD44780, PCD8544, ST7735, HT1621, and
 UC1701X controllers. It provides a Pillow-compatible drawing canvas, and other
 functionality to support:
    scrolling/panning capability,
    terminal-style printing,
    state management,
    color/greyscale (where supported),
    dithering to monochrome
